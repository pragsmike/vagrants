#!/bin/sh
apt-get -y install curl
curl http://debian.neo4j.org/neotechnology.gpg.key | apt-key add -
echo 'deb http://debian.neo4j.org/repo stable/' > /etc/apt/sources.list.d/neo4j.list
apt-get update

apt-get -y install neo4j
sed -i.bak -e "/^#org.neo4j.server.webserver.address=.*/ s/^#//" /etc/neo4j/neo4j-server.properties

service neo4j-service restart

