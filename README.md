Vagrant files

Use these directories with Vagrant to create VMs with particular things installed.

We assume you're using a Windows workstation.

Install these on your workstation:
- [virtualbox](https://www.virtualbox.org/‎)
- [vagrant](http://www.vagrantup.com/)

Clone this repo, then cd to the directory you want.

    vagrant up

    vagrant ssh

    vagrant destroy

Each of these boxes installs and runs some service, and makes it available on some port in localhost.

For neo4j: http://localhost:7474/browser/