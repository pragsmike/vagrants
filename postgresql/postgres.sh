#!/bin/sh
apt-get -y install curl
curl -k https://www.postgresql.org/media/keys/ACCC4CF8.asc | apt-key add -

echo 'deb http://apt.postgresql.org/pub/repos/apt/ precise-pgdg main' > /etc/apt/sources.list.d/pgdg.list
apt-get update

apt-get -y install postgresql-9.3

#sudo -u postgres /usr/lib/postgresql/9.3/bin/psql -c "create user jfw createdb createuser password 'Vinson00';"
#sudo -u postgres /usr/lib/postgresql/9.3/bin/psql -c "create database jfw owner jfw;"
sudo -u postgres /usr/lib/postgresql/9.3/bin/psql -f /vagrant/jfw.sql

echo "listen_addresses = '*'" >> /etc/postgresql/9.3/main/postgresql.conf
echo "host   all    jfw    all    md5" >> /etc/postgresql/9.3/main/pg_hba.conf

service postgresql restart