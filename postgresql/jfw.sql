--
-- PostgreSQL database cluster dump
--

-- Started on 2014-04-02 10:03:01

SET client_encoding = 'LATIN1';
SET standard_conforming_strings = on;

--
-- Roles
--

create user jfw createdb createuser password 'Vinson00';

--
-- Database creation
--

CREATE DATABASE jfw OWNER jfw;

\connect jfw

--
-- PostgreSQL database dump
--

-- Dumped from database version 9.3.4
-- Dumped by pg_dump version 9.3.1
-- Started on 2014-04-02 10:03:01

SET statement_timeout = 0;
SET lock_timeout = 0;
SET client_encoding = 'LATIN1';
SET standard_conforming_strings = on;
SET check_function_bodies = false;
SET client_min_messages = warning;

--
-- TOC entry 7 (class 2615 OID 16391)
-- Name: jfw; Type: SCHEMA; Schema: -; Owner: jfw
--

CREATE SCHEMA jfw;
ALTER SCHEMA jfw OWNER TO jfw;

--
-- TOC entry 8 (class 2615 OID 16392)
-- Name: jopes; Type: SCHEMA; Schema: -; Owner: jfw
--

CREATE SCHEMA jopes;
ALTER SCHEMA jopes OWNER TO jfw;

SET search_path = jopes, pg_catalog;
SET default_tablespace = '';
SET default_with_oids = false;

--
-- TOC entry 172 (class 1259 OID 16497)
-- Name: geolocation; Type: TABLE; Schema: jopes; Owner: postgres; Tablespace: 
--

CREATE TABLE geolocation (
    geolocationcode character varying(4) NOT NULL,
    installationtypecode character varying(3) NOT NULL,
    milstampcode character varying(3),
    icaocode character varying(4),
    name character varying(17) NOT NULL,
    codestatus character varying(1),
    countrycode character varying(2) NOT NULL,
    createdate timestamp with time zone NOT NULL,
    updatedate timestamp with time zone NOT NULL,
    cancelleddate timestamp with time zone,
    cityname character varying(4),
    encyclopediaid character varying(10),
    geolocationcinc character varying(1),
    latitude character varying(8),
    longitude character varying(8),
    longname character varying(35),
    lprcode character varying(2),
    militarygeolocationcode character varying(4),
    reportingunitid character varying(6),
    securityclassificationcode character varying(1)
);

ALTER TABLE jopes.geolocation OWNER TO postgres;

--
-- TOC entry 1862 (class 2606 OID 16502)
-- Name: geolocation_pkey; Type: CONSTRAINT; Schema: jopes; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY geolocation
    ADD CONSTRAINT geolocation_pkey PRIMARY KEY (geolocationcode);
